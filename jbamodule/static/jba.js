//javascript file for jba
( function ( $ ) {
	// Add click event handler to button
	$( '#load_depths' ).click( function () {
		if ( ! window.FileReader ) {
			return alert( 'FileReader API is not supported by your browser.' );
		}
		var $i = $( '#browse_depths' ), // Put file input ID here
			input = $i[0]; // Getting the element from jQuery
		if ( input.files && input.files[0] ) {
			file = input.files[0]; // The file
			fr = new FileReader(); // FileReader instance
			fr.onload = function () {
				// Do stuff on onload, use fr.result for contents of file
				//$( '#file-content' ).append( $( '<div/>' ).html( fr.result ) )
        arr = parsecsv(fr.result);
        html_string = "";
        console.log("min: ", d3.min(arr));
        console.log("max: ", d3.max(arr));
        console.log("count: ", arr.length);
        html_string = html_string.concat("min: ", d3.min(arr),"<br>","max: ", d3.max(arr), "<br>","count: ", arr.length) ;
        console.log(html_string);
        $('#file-statistics').append(html_string);
			};
			fr.readAsText( file );
			//fr.readAsDataURL( file );
		} else {
			// Handle errors here
			alert( "File not selected or browser incompatible." )
		}
	} );
} )( jQuery );

$(document).on('change', '#browse_depths', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        if ( ! window.FileReader ) {
    			return alert( 'FileReader API is not supported by your browser.' );
    		}
    		var $i = $( '#browse_depths' ), // Put file input ID here
    			input = $i[0]; // Getting the element from jQuery
    		if ( input.files && input.files[0] ) {
    			file = input.files[0]; // The file
    			fr = new FileReader(); // FileReader instance
    			fr.onload = function () {
            arr = parsecsv(fr.result);
            $("#file-statistics").empty();
            html_string = "";
            html_string = html_string.concat("Min: ", d3.min(arr),"<br>","Max: ", d3.max(arr), "<br>","Count: ", arr.length) ;
            html_string = html_string.concat("<br><br>" , "Filename: ", label);
            $('#file-statistics').append(html_string);
    			};
    			fr.readAsText( file );
    		} else {
    			alert( "File not selected or browser incompatible." )
    		}
});


$(document).on('change', '#browse_vulnerabilityCurve', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        if ( ! window.FileReader ) {
    			return alert( 'FileReader API is not supported by your browser.' );
    		}
    		var $i = $( '#browse_vulnerabilityCurve' ), // Put file input ID here
    			input = $i[0]; // Getting the element from jQuery
    		if ( input.files && input.files[0] ) {
    			file = input.files[0]; // The file
    			fr = new FileReader(); // FileReader instance
    			fr.onload = function () {
          //datafile = JSON.parse(fr.result);
          var datafile = d3.csvParse(fr.result);

          var mydata = eval(datafile);

          $("#TableCont").append("<table id='testTable' class='table'></table>");

          // Spawn a new JSONTable object on the newly created table
          var jsonTable = new JSONTable($("#testTable"));

          // Create HTML table (data)structure from JSON data
          jsonTable.fromJSON(mydata);
          InitChart(mydata);
    			};
    			fr.readAsText( file );
    		} else {
    			alert( "File not selected or browser incompatible." )
    		}
});


function parsecsv(csv){
    depths = []
    var parsedCSV = d3.csvParse(csv, function(d){
      depths.push(Number(d["Depth (m)"]));
    });
    return depths;
}


function creategraph(elements){
  //console.log(elements);
  var data = [];
  $.each(elements, function( index, value ) {
      x = value.max;
      y = value.cost;
      data[index] = {x, y};
    });
  console.log(data);
}


function InitChart(series) {

    var data = series;
    var vis = d3.select("#visualisation"),
        WIDTH = 450,
        HEIGHT = 300,
        MARGINS = {
            top: 20,
            right: 50,
            bottom: 20,
            left: 70
        },
        xScale = d3.scaleLinear().range([MARGINS.left, WIDTH - MARGINS.right]).domain([0, 11]),
        yScale = d3.scaleLinear().range([HEIGHT - MARGINS.top, MARGINS.bottom]).domain([0, 140000]),
        xAxis = d3.axisBottom()
        .scale(xScale),
        yAxis = d3.axisLeft()
        .scale(yScale)
        //.orient("left");

    vis.append("svg:g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + (HEIGHT - MARGINS.bottom) + ")")
        .call(xAxis);
    vis.append("svg:g")
        .attr("class", "y axis")
        .attr("transform", "translate(" + (MARGINS.left) + ",0)")
        .call(yAxis);
    var lineGen = d3.line()
        .x(function(d) {
            return xScale(d.max);
        })
        .y(function(d) {
            return yScale(d.cost);
        })
        //.interpolate("basis");
        .curve(d3.curveLinear);
    vis.append('svg:path')
        //.attr('d', lineGen(data))
        .attr('d', lineGen(data))
        .attr('stroke', 'green')
        .attr('stroke-width', 2)
        .attr('fill', 'none');
}


$(function() {
    $('#calculateRisk').click(function() {
        var user = $('#txtUsername').val();
        var pass = $('#txtPassword').val();
				ajaxData = new FormData();
				ajaxData.append('file', $("#browse_depths")[0].files[0], 'depths.csv');
				ajaxData.append('file', $("#browse_vulnerabilityCurve")[0].files[0], 'v_curve.csv');

				var xhr = new XMLHttpRequest();
				xhr.open('POST', '/results/', true);

				xhr.onload = function () {
  				if (xhr.status === 200) {
    				// File(s) uploaded.
						//console.log('firt step ok');
						$("#resultsTable").append("<table id='resultTable' class='table'></table>");
						$("#resultTable").empty();
						$("#resultTable").append("");
						//$("#resultsTable").append(xhr.response);
						//var jsonTable = new JSONTable($("#resultTable"));
						mydata = JSON.parse(xhr.response);
						$.each(mydata, function(key, item) {
								if (key == 'Damage') {
									$("#resultTable").prepend("<tr><td>" + key +
										"</td><td>" + item.toLocaleString('EN-UK', {style: 'currency', currency: 'GBP'}) +
									 	"</td></tr>");
								}
								else {
						 			$("#resultTable").append("<tr><td>" + key + "</td><td>" + item + "</td></tr>");
							}
    				 	console.log(key, item);
						 });
						//jsonTable.fromJSON(mydata);
						//console.log(typeof(mydata));

						// Create HTML table (data)structure from JSON data
						//console.log(xhr.response);
            //console.log(xhr.responseText)
						//uploadButton.innerHTML = 'Upload';
  				} else {
    				alert('An error occurred!');
  				}
				};
				xhr.send(ajaxData);
    });
});

// $("#resultsTable").append("<table id='testTable' class='table'></table>")
// var jsonTable = new JSONTable($("#testTable"));
// // Create HTML table (data)structure from JSON data
// jsonTable.fromJSON(mydata);



// function calculateRisk() {
//
// 	ajaxData = new FormData();
// 	//console.log(ajaxData);
//   //ajaxData.append( 'action','uploadFiles');
// 	ajaxData.append('depths', $("#browse_depths")[0].files);
// 	ajaxData.append('vulnerability_curve', $("#browse_vulnerabilityCurve")[0].files);
// 	//return ajaxData
//
// 	$.ajax({
//                 url: "/result/",
//                 type: "post",
//                 data: ajaxData,
//                 success: function(d) {
//                     alert(d);
//                 }
//             });
//
// }
