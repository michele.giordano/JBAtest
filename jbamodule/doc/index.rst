.. Risk assessment library documentation master file, created by
   sphinx-quickstart on Thu Sep 28 11:51:05 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Risk assessment library's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   code



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
