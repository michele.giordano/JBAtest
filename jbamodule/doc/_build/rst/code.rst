
risk_assessment.py documentation
********************************

**risk_assessment.riskAssessment(depth, v_curve)**

   Calculate the risk assessment and its execution time

   :Parameters:
      * **depth** (*csv file**(**float**)** See
        static/data_sample.zip*) – depth values

      * **v_curve** (*csv file**(**float**)** See
        static/data_sample.zip*) – vulnerability curve values

   :Returns:
      ‘Damage’, ‘Computation time’, ‘Read file time’, ‘Total time’

   :Return type:
      Dict

   :Example:
   ‘Usage: python risk_assessment.py <depths.csv>
   <vulnerability_curve.csv> –plot <filename>’

   ‘Usage: >>>from risk_assessment import riskAssessment, vrPlot’
      >>>ra = riskAssessment(‘depths.csv’,’vulenrability_curve.csv’)

   Note: check the arguments data structure in data_sample.zip

**risk_assessment.vrPlot(vr_curve, filename)**

   Draws and save the vulnerability_curve plot

   :Parameters:
      * **depth** – depth values

      * **v_curve** (*csv file**, **float**, **See data_sample.zip*) –
        output image filename

   :Returns:
      image on fs

   :Return type:
      file

   :Example:
   ‘Usage: >>>from risk_assessment import vrPlot’
      >>>ra = vrPlot(‘vulenrability_curve.csv’, ‘plot.png’)

   Note: check the arguments data structure in data_sample.zip
