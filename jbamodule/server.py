# Copyright <2017> <MIT LICENSE>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import os
import locale
import time
import numpy as np
from flask import Flask, jsonify, flash, render_template, request, redirect, url_for, Response
from flask_login import LoginManager, UserMixin, login_required, login_user, logout_user
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_restful import reqparse, abort, Api, Resource
from werkzeug.utils import secure_filename
from risk_assessment import riskAssessment, vrPlot


ALLOWED_EXTENSIONS = set(['csv'])
UPLOAD_FOLDER = '/var/www/jba/upload/'
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///jba.db'
db = SQLAlchemy(app)
api = Api(app)
Bootstrap(app)

app.secret_key = "45fse4f879yg"

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"


@app.route('/ra/', methods=['GET'])
@login_required
def test():
    return render_template('riskassessment.html')

@login_manager.user_loader
def user_loader(user_id):
    """Given *user_id*, return the associated User object.

    :param unicode user_id: user_id (email) user to retrieve
    """
    return User.query.get(user_id)

def allowed_file(filename):
    return '.' in filename and \
            filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
    # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename

        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filename = str(int(time.time())) + filename
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            depths_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            vulnerability_curve = [(0,0,0),(0,1,50000),(1,2,80000),(2,3,95000),(3,4,105000),(4,5,112000),(5,6,120000),(6,7,125000),(7,8,130000),(8,9,132500),(9,10,134000)]
            #res = results(depths_path, vulnerability_curve)
            res = "ok file uploadato"
            return jsonify(res)
        return redirect(request.url)
    if request.method == 'GET':
        return render_template('home.html')

class User(db.Model):
    """An admin user capable of viewing reports.

    :param str email: email address of user
    :param str password: encrypted password for the user

    """
    __tablename__ = 'user'

    email = db.Column(db.String, primary_key=True)
    password = db.Column(db.String)
    authenticated = db.Column(db.Boolean, default=False)

    def is_active(self):
        """True, as all users are active."""
        return True

    def get_id(self):
        """Return the email address to satisfy Flask-Login's requirements."""
        return self.email

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.authenticated

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False


@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    elif request.method == 'POST':
        username = request.form['username']
        user = User.query.get(username)
        if user:
            if (user.password == request.form['password']):
                user.authenticated = True
                db.session.add(user)
                db.session.commit()
                login_user(user, remember=True)
                return redirect(request.args.get("next"))
        return redirect('login')


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect('/ra/')



@app.route("/")
@login_required
def hello():
    return redirect("/ra/")


@app.route('/results/', methods = ['POST'])
@login_required
def api_calculate():
    if request.method == 'POST':
        timestamp = str(int(time.time()))
        depths_file = ""
        vc_curve = ""
        i=0
        for  afile in request.files.getlist('file'):
            if i == 0:
                filename = timestamp + "depths.csv"
                depths_file = os.path.join(app.config['UPLOAD_FOLDER'], filename)
                i +=1
            else:
                filename = timestamp + "vc_curve.csv"
                vc_curve = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            app.logger.debug(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            afile.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))


        res = riskAssessment(depths_file, vc_curve)
        app.logger.debug(res)
        return jsonify(res)


if __name__ == '__main__':
    app.run(debug=True)
