import os
import sys
import time
import csv
import numpy as np
import matplotlib.pyplot as plt


def riskAssessment(depth, v_curve):
    """Calculate the risk assessment and its execution time

        :param depth: depth values
        :param v_curve: vulnerability curve values
        :type depth: csv file(float). See static/data_sample.zip
        :type v_curve: csv file(float). See static/data_sample.zip
        :returns: 'Damage', 'Computation time', 'Read file time', 'Total time'
        :rtype: Dict

        :Example:

        'Usage: python risk_assessment.py <depths.csv> <vulnerability_curve.csv> --plot <filename>'

        'Usage: >>>from risk_assessment import riskAssessment, vrPlot'
                >>>ra = riskAssessment('depths.csv','vulenrability_curve.csv')

        .. note:: check the arguments data structure in data_sample.zip

        """

    start_read_time = time.time()

    try:
        csvtable = np.array(list(csv.reader(open(v_curve, "rb"), delimiter=",")))
        depths = np.genfromtxt(depth,delimiter=',',skip_header=1)
    except Exception, e:
        print (e)
        sys.exit(1)

    vulnerability_curve = (csvtable[1:,1:4]).astype(int)
    end_read_time = time.time()-start_read_time
    start_time = time.time()
    mydata_reclass = np.empty(depths.shape)

    i=0
    while(i<len(vulnerability_curve)):
        mydata_reclass[np.where((depths > vulnerability_curve[i][0]) & (depths <= vulnerability_curve[i][1]))] = vulnerability_curve[i][2]
        i += 1

    expected_damage = np.sum(mydata_reclass)
    endtime = time.time() - start_time

    return {'Damage': expected_damage, 'Computation time': endtime, \
            'Read file time': end_read_time, 'Total time': endtime+end_read_time}


def vrPlot(vr_curve, filename):
    """Draws and save the vulnerability_curve plot

        :param depth: depth values
        :param v_curve: output image filename
        :type v_curve: csv file, float, See data_sample.zip
        :returns: image on fs
        :rtype: file


        :Example:


        'Usage: >>>from risk_assessment import vrPlot'
                >>>ra = vrPlot('vulenrability_curve.csv', 'plot.png')

        .. note:: check the arguments data structure in data_sample.zip

        """

    try:
        csvtable = np.array(list(csv.reader(open(vr_curve, "rb"), delimiter=",")))
        vulnerability_curve = (csvtable[1:,1:4]).astype(int)
    except Exception, e:
        print (e)
        #print >> sys.stderr, "Exception: %s" % str(e)
        sys.exit(1)

    fig = plt.figure()
    plt.plot(vulnerability_curve)
    plt.title("Vulnerability Curve")
    plt.xlabel('Depth (meters)')
    plt.ylabel('Damage (pounds)')
    #plt.show()
    fig.savefig(filename)

    return


if __name__ == "__main__":
    if len(sys.argv) < 4:
        print "Usage: python risk_assessment.py <depths.csv> <vulnerability_curve.csv> --plot <filename>"
    if (len(sys.argv) > 3) and str(sys.argv[3] == "--plot"):
        risk = riskAssessment(str(sys.argv[1]), str(sys.argv[2]))
        print risk
        vrPlot(str(sys.argv[2]), str(sys.argv[4]))
    else:
        risk = riskAssessment(str(sys.argv[1]), str(sys.argv[2]))
        print risk

