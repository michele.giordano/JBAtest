import sys, os, pytest

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from risk_assessment import riskAssessment, vrPlot

def test_riskAssessment():
    result = riskAssessment('data_sample/depths.csv','data_sample/vulnerability_curve.csv')
    damage = result.get('Damage')
    assert damage == 812722500.0