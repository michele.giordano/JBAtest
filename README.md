# JBA Risk Assessment library

This module assesses the risk in case of flooding

## Getting Started

This library can be used as standalone module from shell or as library.

Check the [documentation](https://gitlab.com/michele.giordano/JBAtest/blob/master/jbamodule/doc/_build/rst/index.rst).


The web interface to test it at http://rs.progis.co.uk/


You can also see the proof of concept looking at [Ipython notebook](https://gitlab.com/michele.giordano/JBAtest/blob/master/IPython_notebook/jba.ipynb)



[Source Code](https://gitlab.com/michele.giordano/JBAtest/tree/master/jbamodule)



### Prerequisites

The software is in Python, you can check the requirements in the requirements.txt.
The web Interface is written using [Flask](http://flask.pocoo.org/): "...a microframework for Python based on Werkzeug, Jinja 2 and good intentions".
You can check how the library works looking at this [notebook](https://gitlab.com/michele.giordano/JBAtest/blob/master/IPython_notebook/.ipynb_checkpoints/jba-checkpoint.ipynb)
Sample data can be found on the folder './data_sample'



### Installing

clone this repository to favourite python environment and install all dependencies using:

```
pip install -r requirements.txt

```

```
git clone https://gitlab.com/michele.giordano/JBAtest.git

```


### Usage

```
python risk_assessment.py <depths.csv> <vulnerability_curve.csv> --plot <namefile>

```


## Running the tests

```
cd test/
pytest test_risk_assessment.py

```


